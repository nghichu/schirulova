package com.rezdy.lunch;

import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.rezdy.lunch.entity.Lunch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class LunchTests extends AbstractTest {

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@SuppressWarnings("unchecked")
	@Test
	public void shouldReturnThreeRecipes() throws Exception {
		String uri = "/api/lunch/2020-09-16";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		Map<String, List<Lunch>> map = super.mapFromJson(content, HashMap.class);
		List<Lunch> recipes = map.get("recipes");
		assertTrue(recipes.size() == 3);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void shouldReturnTwoRecipes() throws Exception {
		String uri = "/api/lunch/2020-09-27";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		Map<String, List<Lunch>> map = super.mapFromJson(content, HashMap.class);
		List<Lunch> recipes = map.get("recipes");
		assertTrue(recipes.size() == 2);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void shouldReturnOneRecipes() throws Exception {
		String uri = "/api/lunch/2020-09-29";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		Map<String, List<Lunch>> map = super.mapFromJson(content, HashMap.class);
		List<Lunch> recipes = map.get("recipes");
		assertTrue(recipes.size() == 1);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void shouldReturnZeroRecipes() throws Exception {
		String uri = "/api/lunch/2020-10-04";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		String content = mvcResult.getResponse().getContentAsString();
		Map<String, List<Lunch>> map = super.mapFromJson(content, HashMap.class);
		List<Lunch> recipes = map.get("recipes");
		assertTrue(recipes.size() == 0);
	}
	
	@Test
	public void shouldReturnNotFound() throws Exception {
		String uri = "/api/lunch/2020/10/04";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(404, status);
		assertTrue(mvcResult.getResponse().getContentAsString().isEmpty());
	}
}
