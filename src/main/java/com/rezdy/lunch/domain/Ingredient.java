package com.rezdy.lunch.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ingredients")
public class Ingredient {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false)
	private String title;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date best_before;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date use_by;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getBest_before() {
		return best_before;
	}

	public void setBest_before(Date best_before) {
		this.best_before = best_before;
	}

	public Date getUse_by() {
		return use_by;
	}

	public void setUse_by(Date use_by) {
		this.use_by = use_by;
	}

}
