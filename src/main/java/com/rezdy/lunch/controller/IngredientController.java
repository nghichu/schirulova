package com.rezdy.lunch.controller;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rezdy.lunch.domain.Ingredient;
import com.rezdy.lunch.domain.Recipe;
import com.rezdy.lunch.domain.RecipeIngredient;
import com.rezdy.lunch.entity.Lunch;
import com.rezdy.lunch.repo.IngredientRepository;
import com.rezdy.lunch.repo.RecipeIngredientRepository;
import com.rezdy.lunch.repo.RecipeRepository;
import com.rezdy.lunch.util.DateValidator;

@RestController
@RequestMapping("/api")
public class IngredientController {
	@Autowired
	IngredientRepository ingredientRepository;

	@Autowired
	RecipeRepository recipeRepository;

	@Autowired
	RecipeIngredientRepository recipeIngredientRepository;

	// Get lunch
	@GetMapping("/lunch/{today}")
	public Map<String, List<Lunch>> getLunches(@PathVariable(value = "today") String today) throws ParseException {
		DateValidator validator = new DateValidator("yyyy-MM-dd");
		Map<String, List<Lunch>> resMap = new HashMap<>();
		List<Lunch> lunches = new ArrayList<>();

		// Check if input today is valid with format yyyy-MM-dd
		if (validator.isValid(today)) {
			Ingredient ingredient;
			String recipeTitle;

			// Convert parameter to Date type
			LocalDate localDate = LocalDate.parse(today);
			Date currentDate = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());

			// Get all ingredients that have use_by is after input date
			// Use map to avoid possible nested loop in the following code
			Map<Long, Ingredient> ingredientMap = ingredientRepository.findAllBeforeDate(today).stream()
					.collect(Collectors.toMap(Ingredient::getId, i -> i));

			// Get all recipes from database
			// Use map to avoid possible nested loop in the following code
			Map<Long, Recipe> recipeMap = recipeRepository.findAll().stream()
					.collect(Collectors.toMap(Recipe::getId, recipe -> recipe));

			// Get all recipe-ingredients
			// Grouping by recipe ID so that we are easily can know which
			// ingredients that each recipe has
			Map<Long, List<Long>> recipeIngredientMap = recipeIngredientRepository.findAll().stream()
					.collect(Collectors.groupingBy(RecipeIngredient::getRecipe_id,
							Collectors.mapping(RecipeIngredient::getIngredient_id, Collectors.toList())));

			// Remove all recipes which has some non-existing ingredients
			// And keep the ones which have usable ingredients in the fridge
			Map<Long, List<Long>> map = new HashMap<>();
			for (Long key : recipeIngredientMap.keySet()) {
				if (ingredientMap.keySet().containsAll(recipeIngredientMap.get(key))) {
					map.put(key, recipeIngredientMap.get(key));
				}
			}

			// Loop through the map to build lunches using recipe titles and ingredient titles
			for (Long key : map.keySet()) {
				// Get title of current recipe
				recipeTitle = recipeMap.get(key).getTitle();

				// This flag is use for checking if there is a
				// ingredient of current recipe pasts its best before and still
				// within its use by
				boolean isAfterBestBefore = false;

				List<String> ingredientTitles = new ArrayList<>();
				for (Long ingredientId : map.get(key)) {
					ingredient = ingredientMap.get(ingredientId);

					// Create list of ingredient titles for each recipe
					ingredientTitles.add(ingredient.getTitle());

					// Checking if current ingredient past its best before and
					// still within its use by
					if (ingredient.getUse_by().compareTo(currentDate) >= 0
							&& ingredient.getBest_before().compareTo(currentDate) < 0) {
						isAfterBestBefore = true;
					}
				}

				// Create lunch
				Lunch lunch = new Lunch(recipeTitle, ingredientTitles);

				// If the lunch has an ingredient pasts its best before, put it
				// to the end of the list, otherwise add it to first position of
				// the list.
				if (isAfterBestBefore) {
					lunches.add(lunch);
				} else {
					lunches.add(0, lunch);
				}
			}
			resMap.put("recipes", lunches);
		}
		return resMap;
	}
}
