package com.rezdy.lunch.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateValidator {
	private String dateFormat;

	public DateValidator(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public boolean isValid(String dateStr) throws ParseException {
		DateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setLenient(false);
		try {
			sdf.parse(dateStr);
		} catch (IllegalArgumentException e) {
			ParseException parseException = new ParseException("Invalid date input: " + dateStr, 0);
			parseException.initCause(e);
			throw parseException;
		}
		return true;
	}
}
