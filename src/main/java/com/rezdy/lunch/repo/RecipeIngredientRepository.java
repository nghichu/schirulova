package com.rezdy.lunch.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rezdy.lunch.domain.RecipeIngredient;

public interface RecipeIngredientRepository extends JpaRepository<RecipeIngredient, Long> {

}
