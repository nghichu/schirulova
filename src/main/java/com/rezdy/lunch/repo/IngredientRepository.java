package com.rezdy.lunch.repo;

import com.rezdy.lunch.domain.Ingredient;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IngredientRepository extends JpaRepository<Ingredient, Long> {

	@Query(value = "select * from ingredients i where i.use_by >= :today", nativeQuery = true)
	List<Ingredient> findAllBeforeDate(@Param("today") String today);

}
