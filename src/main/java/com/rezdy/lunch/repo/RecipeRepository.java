package com.rezdy.lunch.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rezdy.lunch.domain.Recipe;

public interface RecipeRepository extends JpaRepository<Recipe, Long> {

}
