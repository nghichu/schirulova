package com.rezdy.lunch.entity;

import java.util.List;

public class Lunch {
	String title;
	List<String> ingredients;

	public Lunch(String title, List<String> ingredients) {
		this.title = title;
		this.ingredients = ingredients;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<String> ingredients) {
		this.ingredients = ingredients;
	}
}
