#Install JDK 8
```
https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html
```

#Install Maven
```
https://maven.apache.org/install.html
```

# Download repository and add project to Eclipse
```
```

# Create schema and data (MySQL)
```
Running lunch/src/main/resources/sql/lunch.sql in MYSQL Workbench
```

# Fix database connection properties
```
In file lunch/src/main/resources/application.properties
```

## Run Maven Install
```
Right click project > Run As > Maven install
```

## Run maven build
```
Right click project > Run As > Maven build
```

## Run application
```
Right click project > Run As > Java Application
```

## Run test
```
Right click on file lunch/src/test/java/com/rezdy/lunch/LunchTests.java > Run As > JUnit (create Run configuration before running)
```

### For your reference of result
```
In folder lunch/src/main/resources/images/
```

### Docker (haven't done yet as my laptop can't install docker because of Windows version)
### But can be refer from the following link
```
https://spring.io/guides/gs/spring-boot-docker/
```